package me.lfelipeeb.empresas;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import org.w3c.dom.Text;

import me.lfelipeeb.empresas.Model.Empresa;

public class EmpresaActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_empresa);

        Empresa empresa = (Empresa) getIntent().getSerializableExtra("empresa");

        TextView description = (TextView) findViewById(R.id.description);
        TextView name = (TextView) findViewById(R.id.enterpriseName);
        ImageView img = (ImageView) findViewById(R.id.logoEnterprise);

        if(empresa.getPhoto() != null){
            Picasso.get().load(empresa.getPhoto()).into(img);
        }

        name.setText(empresa.getEnterpriseName());
        description.setText(empresa.getDescription());

        Toolbar mToolbar = (Toolbar) findViewById(R.id.main_toolbar);
        setSupportActionBar(mToolbar);

        getSupportActionBar().setTitle(null);

        mToolbar.setNavigationIcon(R.drawable.ic_keyboard_backspace_white_24px);
        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }

}
