package me.lfelipeeb.empresas;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;

import java.io.Serializable;
import java.util.Map;

import me.lfelipeeb.empresas.Model.Enterprises;
import me.lfelipeeb.empresas.Util.APIClient;
import me.lfelipeeb.empresas.Util.APIinterface;
import me.lfelipeeb.empresas.Util.EmpresaAdapter;
import retrofit2.Call;
import retrofit2.Callback;

public class MainActivity extends AppCompatActivity {
    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    private Toolbar mToolbar;
    private Map<String, String> parms;
    private String access, client, uid;
    private Enterprises mData;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        parms = (Map<String, String>) getIntent().getSerializableExtra("credentions");
        access = parms.get("access-token");
        client = parms.get("client");
        uid = parms.get("uid");

        Enterprises mData = listEmpresas();

        mToolbar = (Toolbar) findViewById(R.id.main_toolbar);
        setSupportActionBar(mToolbar);

        getSupportActionBar().setTitle(null);

        mToolbar.setNavigationIcon(R.drawable.ic_keyboard_backspace_white_24px);
        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(final Menu menu){
        super.onCreateOptionsMenu(menu);

        getMenuInflater().inflate(R.menu.option_menu, menu);

        SearchView searchView = (SearchView) menu.findItem(R.id.search_menu).getActionView();
        searchView.setQueryHint("Pesquisar Empresa");
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                APIinterface apIinterface = APIClient.getClient().create(APIinterface.class);
                Call<Enterprises> call = apIinterface.searchEnterprises(access, client, uid, query);
                call.enqueue(new Callback<Enterprises>() {
                    @Override
                    public void onResponse(Call<Enterprises> call, retrofit2.Response<Enterprises> response) {
                        mData = response.body();
                        mRecyclerView.setAdapter(new EmpresaAdapter(mData.getEnterprises(), MainActivity.this));
                        mAdapter.notifyDataSetChanged();
                    }

                    @Override
                    public void onFailure(Call<Enterprises> call, Throwable t) {
                        Log.d("Response: ", "Error: " + t.getMessage());
                    }

                });


                return true;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                return false;
            }
        });

        searchView.setOnCloseListener(new SearchView.OnCloseListener() {
            @Override
            public boolean onClose() {

                APIinterface apIinterface = APIClient.getClient().create(APIinterface.class);
                Call<Enterprises> call = apIinterface.listEnterprises(access, client, uid);
                call.enqueue(new Callback<Enterprises>() {
                    @Override
                    public void onResponse(Call<Enterprises> call, retrofit2.Response<Enterprises> response) {
                        mData = response.body();
                        mRecyclerView.setAdapter(new EmpresaAdapter(mData.getEnterprises(), MainActivity.this));
                        mAdapter.notifyDataSetChanged();
                    }

                    @Override
                    public void onFailure(Call<Enterprises> call, Throwable t) {
                        Log.d("Response: ", "Error: " + t.getMessage());
                    }

                });

                return true;
            }
        });

        return true;
    }


    private Enterprises listEmpresas(){

        APIinterface apIinterface = APIClient.getClient().create(APIinterface.class);
        Call<Enterprises> call = apIinterface.listEnterprises(access, client, uid);

        call.enqueue(new Callback<Enterprises>() {

            @Override
            public void onResponse(Call<Enterprises> call, retrofit2.Response<Enterprises> response) {
                mData = response.body();
                createRecycleView();
            }

            @Override
            public void onFailure(Call<Enterprises> call, Throwable t) {
                Log.d("Response: ", "Error: " + t.getMessage());
            }

        });

        return mData;
    }


    private RecyclerView createRecycleView(){
        mRecyclerView = (RecyclerView) findViewById(R.id.rv_empresas);


        mLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(mLayoutManager);

        mAdapter = new EmpresaAdapter(mData.getEnterprises(), this);


        mRecyclerView.setAdapter(mAdapter);

        return mRecyclerView;
    }
}
