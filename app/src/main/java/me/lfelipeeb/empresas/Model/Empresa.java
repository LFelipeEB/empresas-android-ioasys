package me.lfelipeeb.empresas.Model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

/**
 * Created by lfelipeeb on 22/03/18.
 */

public class Empresa implements Serializable{

    @SerializedName("id")
    private Integer id;

    @SerializedName("email_enterprise")
    private String email;

    @SerializedName("facebook")
    private String facebook;

    @SerializedName("twitter")
    private String twitter;

    @SerializedName("linkedin")
    private String linkedin;

    @SerializedName("phone")
    private String phone;

    @SerializedName("own_enterprise")
    private String ownEnterprise;

    @SerializedName("enterprise_name")
    private String enterpriseName;

    @SerializedName("photo")
    private String photo;

    @SerializedName("description")
    private String description;


    @SerializedName("city")
    private String city;


    @SerializedName("country")
    private String country;

    @SerializedName("value")
    private String value;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFacebook() {
        return facebook;
    }

    public void setFacebook(String facebook) {
        this.facebook = facebook;
    }

    public String getTwitter() {
        return twitter;
    }

    public void setTwitter(String twitter) {
        this.twitter = twitter;
    }

    public String getLinkedin() {
        return linkedin;
    }

    public void setLinkedin(String linkedin) {
        this.linkedin = linkedin;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getOwnEnterprise() {
        return ownEnterprise;
    }

    public void setOwnEnterprise(String ownEnterprise) {
        this.ownEnterprise = ownEnterprise;
    }

    public String getEnterpriseName() {
        return enterpriseName;
    }

    public void setEnterpriseName(String enterpriseName) {
        this.enterpriseName = enterpriseName;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public Integer getSharePrice() {
        return sharePrice;
    }

    public void setSharePrice(Integer sharePrice) {
        this.sharePrice = sharePrice;
    }

    public Tipo getType() {
        return type;
    }

    public void setType(Tipo type) {
        this.type = type;
    }

    @SerializedName("share_price")

    private Integer sharePrice;

    @SerializedName("enterprise_type")
    private Tipo type;

    @Override
    public String toString() {
        return "Enterprise{" +
                "id=" + id +
                ", email='" + email + '\'' +
                ", facebook='" + facebook + '\'' +
                ", twitter='" + twitter + '\'' +
                ", linkedin='" + linkedin + '\'' +
                ", phone='" + phone + '\'' +
                ", ownEnterprise='" + ownEnterprise + '\'' +
                ", enterpriseName='" + enterpriseName + '\'' +
                ", photo='" + photo + '\'' +
                ", description='" + description + '\'' +
                ", city='" + city + '\'' +
                ", country='" + country + '\'' +
                ", value='" + value + '\'' +
                ", sharePrice=" + sharePrice +
                ", type=" + type +
                '}';
    }

    public class Tipo implements Serializable{
        @SerializedName("id")
        private Integer id;

        public Integer getId() {
            return id;
        }

        public String getEnterpriseTypeName() {
            return enterpriseTypeName;
        }

        public void setEnterpriseTypeName(String enterpriseTypeName) {
            this.enterpriseTypeName = enterpriseTypeName;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        @SerializedName("enterprise_type_name")
        private String enterpriseTypeName;

    }
}
