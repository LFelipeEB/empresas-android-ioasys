package me.lfelipeeb.empresas.Model;

import com.google.gson.annotations.SerializedName;

import java.util.Arrays;
import java.util.List;

/**
 * Created by lfelipeeb on 24/03/18.
 */

public class Enterprises {

    @SerializedName("enterprises")
    private Empresa[] enterprises;

    @Override
    public String toString() {
        return "Enterprises{" +
                "enterprises=" + Arrays.toString(enterprises) +
                '}';
    }

    public Empresa[] getEnterprises() {
        return enterprises;
    }

    public void setEnterprises(Empresa[] enterprises) {
        this.enterprises = enterprises;
    }

}
