package me.lfelipeeb.empresas.Model;

import com.google.gson.annotations.SerializedName;

import java.lang.reflect.Array;
import java.util.List;

/**
 * Created by lfelipeeb on 22/03/18.
 */

public class User {

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public String getEnterprise() {
        return enterprise;
    }

    public void setEnterprise(String enterprise) {
        this.enterprise = enterprise;
    }

    public Investor getInvestor() {
        return investor;
    }

    public void setInvestor(Investor investor) {
        this.investor = investor;
    }

    @SerializedName("success")
    private Boolean success;

    @SerializedName("enterprise")
    private String enterprise;

    @SerializedName("investor")
    private Investor investor;

    @Override
    public String toString() {
        return "User{" +
                "success=" + success +
                ", enterprise='" + enterprise + '\'' +
                ", investor=" + investor +
                '}';
    }

    public class Investor {
        @SerializedName("investor_name")
        private String investor_name;
        @SerializedName("email")
        private String email;
        @SerializedName("city")
        private String city;
        @SerializedName("country")
        private String country;
        @SerializedName("balance")
        private String balance;
        @SerializedName("photo")
        private String photo;
//        @SerializedName("portfolio")
//        private List<Portifolio> portifolio = null;
        @SerializedName("portfolio_value")
        private String portfolio_value;
        @SerializedName("first_access")
        private Boolean first_access;
        @SerializedName("super_angel")
        private Boolean super_angel;
        @SerializedName("enterprise")
        private Boolean enterprise;


        public String getInvestor_name() {
            return investor_name;
        }

        public void setInvestor_name(String investor_name) {
            this.investor_name = investor_name;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getCity() {
            return city;
        }

        public void setCity(String city) {
            this.city = city;
        }

        public String getCountry() {
            return country;
        }

        public void setCountry(String country) {
            this.country = country;
        }

        public String getBalance() {
            return balance;
        }

        public void setBalance(String balance) {
            this.balance = balance;
        }

        public String getPhoto() {
            return photo;
        }

        public void setPhoto(String photo) {
            this.photo = photo;
        }

//        public List<Portifolio> getPortifolio() {
//            return portifolio;
//        }

//        public void setPortifolio(List<Portifolio> portifolio) {
//            this.portifolio = portifolio;
//        }

        public String getPortfolio_value() {
            return portfolio_value;
        }

        @Override
        public String toString() {
            return "Investor{" +
                    "investor_name='" + investor_name + '\'' +
                    ", email='" + email + '\'' +
                    ", city='" + city + '\'' +
                    ", country='" + country + '\'' +
                    ", balance='" + balance + '\'' +
                    ", photo='" + photo + '\'' +
//                    ", portifolio=" + portifolio +
                    ", portfolio_value='" + portfolio_value + '\'' +
                    ", first_access=" + first_access +
                    ", super_angel=" + super_angel +
                    ", enterprise=" + enterprise +
                    '}';
        }

        public void setPortfolio_value(String portfolio_value) {
            this.portfolio_value = portfolio_value;
        }

        public Boolean getFirst_access() {
            return first_access;
        }

        public void setFirst_access(Boolean first_access) {
            this.first_access = first_access;
        }

        public Boolean getSuper_angel() {
            return super_angel;
        }

        public void setSuper_angel(Boolean super_angel) {
            this.super_angel = super_angel;
        }

        public Boolean getEnterprise() {
            return enterprise;
        }

        public void setEnterprise(Boolean enterprise) {
            this.enterprise = enterprise;
        }

        class Portifolio{
            @SerializedName("enterprises_number")
            private Integer enterprises_number;

            public Integer getEnterprises_number() {
                return enterprises_number;
            }

            public void setEnterprises_number(Integer enterprises_number) {
                this.enterprises_number = enterprises_number;
            }
        }


    }
}
