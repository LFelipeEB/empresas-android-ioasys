package me.lfelipeeb.empresas.Util;

import me.lfelipeeb.empresas.Model.Enterprises;
import me.lfelipeeb.empresas.Model.User;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Query;

/**
 * Created by lfelipeeb on 22/03/18.
 */

public interface APIinterface {

    @FormUrlEncoded
    @POST("users/auth/sign_in")
    Call<User> signIn(
            @Field("email") String email,
            @Field("password") String password
    );


    @GET("enterprises")
    Call<Enterprises> listEnterprises(@Header("access-token") String access_token, @Header("client") String client, @Header("uid") String uid);

    @GET("enterprises")
    Call<Enterprises> searchEnterprises(@Header("access-token") String access_token, @Header("client") String client, @Header("uid") String uid, @Query("name") String name);

}
