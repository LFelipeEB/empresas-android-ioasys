package me.lfelipeeb.empresas.Util;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.io.Serializable;
import java.util.Arrays;
import java.util.List;

import me.lfelipeeb.empresas.EmpresaActivity;
import me.lfelipeeb.empresas.Model.Empresa;
import me.lfelipeeb.empresas.R;

/**
 * Created by lfelipeeb on 22/03/18.
 */

public class EmpresaAdapter extends RecyclerView.Adapter<EmpresaAdapter.ViewHolder>  {
    private List<Empresa> mDataset;
    private Context ctx;

    public EmpresaAdapter(Empresa[] mDataSet, Context ctx){
        this.mDataset = Arrays.asList(mDataSet);
        this.ctx = ctx;
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.adapter_empresa, parent, false);

        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        final Empresa enterprise = mDataset.get(position);

        holder.name.setText(enterprise.getEnterpriseName());
        holder.country.setText(enterprise.getCountry());
        holder.type.setText(enterprise.getType().getEnterpriseTypeName());

        if(enterprise.getPhoto() != null){
            Picasso.get().load(enterprise.getPhoto()).into(holder.image);
        }

        holder.itemView.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                Intent it = new Intent(ctx, EmpresaActivity.class);
                it.putExtra("empresa", enterprise);
                ctx.startActivity(it);
            }
        });

    }

    @Override
    public int getItemCount() {
        return mDataset==null? 0 : mDataset.size();
    }


    public static class ViewHolder extends RecyclerView.ViewHolder {

        private View v;

        public TextView name;
        public TextView type;
        public TextView country;
        public ImageView image;

        public ViewHolder(View v) {
            super(v);

            name = (TextView) v.findViewById(R.id.tvNomeEmpresa);
            type = (TextView) v.findViewById(R.id.tvTypeEmpresa);
            country = (TextView) v.findViewById(R.id.tvPaisEmpresa);
            image = (ImageView) v.findViewById(R.id.imgEmpresa);

        }

    }

}
